%global soversion_major 1
%global soversion 1.0.41
%global _hardened_build 1

Summary: Libcgroup is a library that abstracts the control group file system in Linux
Name: libcgroup
Version: 0.41
Release: 23
License: LGPLv2+
URL: http://libcg.sourceforge.net/
Source0: http://downloads.sourceforge.net/libcg/%{name}-%{version}.tar.bz2
Source1: cgconfig.service
Provides: libcgroup-pam libcgroup-tools
Obsoletes: libcgroup-pam libcgroup-tools

Patch0: config.patch
Patch1: libcgroup-0.37-chmod.patch
Patch2: libcgroup-0.40.rc1-coverity.patch
Patch3: libcgroup-0.40.rc1-fread.patch
Patch4: libcgroup-0.40.rc1-templates-fix.patch
Patch5: libcgroup-0.41-lex.patch
Patch6: libcgroup-0.41-api.c-fix-order-of-memory-subsystem-parameters.patch
Patch7: libcgroup-0.41-api.c-preserve-dirty-flag.patch
Patch8: libcgroup-0.41-change-cgroup-of-threads.patch
Patch9: libcgroup-0.41-fix-infinite-loop.patch
Patch10: libcgroup-0.41-prevent-buffer-overflow.patch
Patch11: libcgroup-0.41-tasks-file-warning.patch
Patch12: libcgroup-0.41-fix-log-level.patch
Patch13: libcgroup-0.41-size-of-controller-values.patch
Patch14: libcgroup-0.41-CVE-2018-14348.patch
Patch9000: bugfix-change-parser-match-order-fix-cgconfig-error.patch

BuildRequires: gcc,gcc-c++,byacc
BuildRequires: systemd-units,pam-devel,flex,coreutils

Requires: systemd >= 217-0.2
Requires(pre): shadow-utils

%description
Cgroups is a Linux kernel feature that limits, accounts for, and isolates
the resource usage (CPU, memory, disk I/O, network, etc.) of a collection of processes.
The library helps manipulte and administrate control groups.

%package devel
Summary: Devel helps applications to use cgroups
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
Devel provides API for creating,deleting and modifying cgroup nodes.It allows
the creation of cgroups' configuration and provides scripts for managing it.

%package help
Summary: It provides helpful information for libcgroup
%description help
It provides helpful information for libcgroup-pam,libcgroup-devel,libcgroup-tools and libcgroup.

%prep
%setup  -q  -n %{name}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p2
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%patch14 -p1
%patch9000 -p1

%build
%configure --enable-pam-module-dir=%{_libdir}/security --enable-opaque-hierarchy="name=systemd" --disable-daemon
make %{?_smp_mflags}

%install
make DESTDIR=$RPM_BUILD_ROOT install

# config
install -d ${RPM_BUILD_ROOT}%{_sysconfdir}
install -d ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
install -m 644 samples/cgconfig.conf $RPM_BUILD_ROOT%{_sysconfdir}/cgconfig.conf
install -m 644 samples/cgsnapshot_blacklist.conf $RPM_BUILD_ROOT%{_sysconfdir}/cgsnapshot_blacklist.conf

# Only one pam_cgroup.so is needed
mv -f $RPM_BUILD_ROOT%{_libdir}/security/pam_cgroup.so.*.*.* $RPM_BUILD_ROOT%{_libdir}/security/pam_cgroup.so
rm -f $RPM_BUILD_ROOT%{_libdir}/security/pam_cgroup.so.*
rm -f $RPM_BUILD_ROOT%{_libdir}/security/pam_cgroup.la
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

rm -f $RPM_BUILD_ROOT%{_mandir}/man5/cgred.conf.5*
rm -f $RPM_BUILD_ROOT%{_mandir}/man5/cgrules.conf.5*
rm -f $RPM_BUILD_ROOT%{_mandir}/man8/cgrulesengd.8*

# unit and sysconfig
install -d ${RPM_BUILD_ROOT}%{_unitdir}
install -m 644 %SOURCE1 ${RPM_BUILD_ROOT}%{_unitdir}/

%pre
getent group cgred >/dev/null || groupadd -r cgred

%post 
%systemd_post cgconfig.service

%preun
%systemd_preun cgconfig.service

%postun
%systemd_postun_with_restart cgconfig.service

%triggerun -- libcgroup < 0.38
/usr/bin/systemd-sysv-convert --save cgconfig >/dev/null 2>&1 ||:
/sbin/chkconfig --del cgconfig >/dev/null 2>&1 || :
/bin/systemctl try-restart cgconfig.service >/dev/null 2>&1 || :

%files
%{!?_licensedir:%global license %%doc}
%license COPYING
%{_libdir}/libcgroup.so.*
%config(noreplace) %{_sysconfdir}/cgsnapshot_blacklist.conf
%config(noreplace) %{_sysconfdir}/cgconfig.conf
/usr/bin/cgget
/usr/bin/cgset
/usr/bin/cgcreate
/usr/bin/cgdelete
/usr/bin/cgsnapshot
/usr/bin/lscgroup
/usr/bin/lssubsys
/usr/sbin/cgclear
/usr/sbin/cgconfigparser
%attr(2755, root, cgred) /usr/bin/cgexec
%attr(2755, root, cgred) /usr/bin/cgclassify
%attr(0755,root,root) %{_libdir}/security/pam_cgroup.so
%{_unitdir}/cgconfig.service

%files devel
%{!?_licensedir:%global license %%doc}
%license COPYING
%{_libdir}/libcgroup.so
%{_libdir}/pkgconfig/libcgroup.pc
%{_includedir}/libcgroup.h
%{_includedir}/libcgroup/*.h

%files help
%license COPYING
%doc README README_systemd
%attr(0644, root, root) %{_mandir}/man1/*
%attr(0644, root, root) %{_mandir}/man5/*
%attr(0644, root, root) %{_mandir}/man8/*

%changelog
* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.41-23
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify spec patch

* Thu Dec 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.41-22
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the config patch

* Thu Nov 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.41-21
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the release

* Tue Jan 22 2019 lunankun<lunankun@huawei.com> - 0.41-20.h1
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:change parser match order fix cgconfig error.

